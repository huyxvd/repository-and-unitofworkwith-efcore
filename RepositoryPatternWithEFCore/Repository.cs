﻿public interface IRepository
{
    public List<Student> GetAllStudent();
    public void AddNewStudent(Student student);
}

public class Repository : IRepository
{
    private readonly AppDbContext _context;
    public Repository(AppDbContext appContext)
    {
        _context = appContext;
    }

    public void AddNewStudent(Student student)
    {
        _context.Students.Add(student);
        _context.SaveChanges();
    }

    public List<Student> GetAllStudent()
    {
        return _context.Students.ToList();
    }
}

public class RepositoryFileLocal : IRepository
{
    public RepositoryFileLocal()
    {
    }

    public void AddNewStudent(Student student)
    {
        Console.WriteLine("Đã ghi vô file thành công");
    }

    public List<Student> GetAllStudent()
    {
        Console.WriteLine("Đã đọc từ file thành công");
        return new List<Student>();
    }
}