﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

IRepository _repository = new Repository(new AppDbContext());
IRepository _repositoryLocalFile = new RepositoryFileLocal();


DemoProgram program = new DemoProgram(_repositoryLocalFile); // Sử dụng local file thay vì db context

program.InsertStudent();


public class DemoProgram
{
    private readonly IRepository _repository;
    public DemoProgram(IRepository repository)
    {
        _repository = repository;
    }

    public void InsertStudent()
    {
        _repository.AddNewStudent(new Student
        {
            FullName = "test"
        });

        _repository.AddNewStudent(new Student
        {
            FullName = "test 2"
        });

        // ... logic của chương trình

        List<Student> students = _repository.GetAllStudent();
        foreach (Student item in students)
        {
            Console.WriteLine($"name: {item.FullName}");
        }
    }
}